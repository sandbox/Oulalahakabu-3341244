Storj

## Storj CDN

This module provide Storj Content Delivery Network integration for Drupal sites.
It alters file URLs, so that files (CSS, JS, images, fonts, videos …) are
downloaded from a CDN instead of your web server.

It does *not* put your entire website behind a CDN.
