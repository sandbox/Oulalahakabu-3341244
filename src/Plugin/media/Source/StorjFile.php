<?php

namespace Drupal\storj\Plugin\media\Source;

use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media\Plugin\media\Source\File;

/**
 * Storj (external) entity media source.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "storj",
 *   label = @Translation("Storj"),
 *   description = @Translation("Use storj files."),
 *   allowed_field_types = {"text_long"},
 *   thumbnail_alt_metadata_attribute = "alt",
 *   default_thumbnail_filename = "no-thumbnail.png"
 * )
 */
class StorjFile extends File {

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('settings', ['file_extensions' => 'mp3 wav aac']);
  }

  /**
   * @inheritDoc
   */
  public function getMetadataAttributes() {
    return [
      static::METADATA_ATTRIBUTE_NAME => $this->t('Name'),
      static::METADATA_ATTRIBUTE_MIME => $this->t('MIME type'),
      static::METADATA_ATTRIBUTE_SIZE => $this->t('File size'),
      'title' => $this->t('Title'),
      'alt_text' => $this->t('Alternative text'),
      'caption' => $this->t('Caption'),
      'credit' => $this->t('Credit'),
      'id' => $this->t('ID'),
      'uri' => $this->t('URL'),
      'width' => $this->t('Width'),
      'height' => $this->t('Height'),
    ];
  }

  /**
   * @inheritDoc
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    switch ($attribute_name) {
      case 'default_name':
        return 'media:' . $media->bundle() . ':' . $media->uuid();

      case 'thumbnail_uri':
        $default_thumbnail_filename = $this->pluginDefinition['default_thumbnail_filename'];
        return $this->configFactory->get('media.settings')->get('icon_base_uri') . '/' . $default_thumbnail_filename;
    }
    return NULL;
  }

  /**
   * Gets the thumbnail image URI based on a file entity.
   *
   * @param \Drupal\file\FileInterface $file
   *   A file entity.
   *
   * @return string
   *   File URI of the thumbnail image or NULL if there is no specific icon.
   */
  protected function getThumbnail(FileInterface $file) {
    $icon_base = $this->configFactory->get('media.settings')->get('icon_base_uri');

    // We try to automatically use the most specific icon present in the
    // $icon_base directory, based on the MIME type. For instance, if an
    // icon file named "pdf.png" is present, it will be used if the file
    // matches this MIME type.
    $mimetype = $file->getMimeType();
    $mimetype = explode('/', $mimetype);

    $icon_names = [
      $mimetype[0] . '--' . $mimetype[1],
      $mimetype[1],
      $mimetype[0],
    ];
    foreach ($icon_names as $icon_name) {
      $thumbnail = $icon_base . '/' . $icon_name . '.png';
      if (is_file($thumbnail)) {
        return $thumbnail;
      }
    }

    return NULL;
  }

  /**
   * Figure out the formatter class to be used on a given media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   The media entity we are interested in.
   *
   * @return string
   *   The FQN of the formatter class configured in the `default` media display
   *   for the media source field.
   */
  public function getFormatterClass(MediaInterface $media) {
    $field_definition = $this->getSourceFieldDefinition($media->bundle->entity);

    // @todo There is probably a better way for this class to figure out what
    // formatter class is being used.
    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display */
    $display = EntityViewDisplay::load('media.' . $media->bundle() . '.default');
    $components = $display->getComponents();
    $formatter_config = $components[$field_definition->getName()] ?? [];
    if (empty($formatter_config['settings']['formatter_class'])) {
      throw new \LogicException('The Remote Media validator needs the _default_ media display to be configured, and for the source field to use any of the formatters provided by the Media Remote module.');
    }
    return $formatter_config['settings']['formatter_class'];
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints() {
    return [
      'media_storj' => [],
    ];
  }

}