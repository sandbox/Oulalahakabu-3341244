<?php

namespace Drupal\storj\Services;

use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\file\FileInterface;
use Drupal\storj\Plugin\media\Source\StorjFile;
use Ramsey\Uuid\Uuid;
use Storj\Uplink\Exception\Object\ObjectNotFound;
use Storj\Uplink\Exception\UplinkException;
use Storj\Uplink\Permission;
use Storj\Uplink\SharePrefix;
use Storj\Uplink\Uplink;

/**
 * Service description.
 */
class Storj {

  /**
   * The config manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * @var \Storj\Uplink\Access[]
   */
  protected $access;

  /**
   * Constructs a Storj object.
   *
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The config manager.
   */
  public function __construct(ConfigManagerInterface $config_manager) {
    $this->configManager = $config_manager;

  }

  /**
   * @param string $satelliteAddress
   * @param string $apiKey
   * @param string $passphrase
   *
   * @return \Storj\Uplink\Access
   * @throws \Storj\Uplink\Exception\UplinkException
   */
  public function initializeUplink(string $satelliteAddress, string $apiKey, string $passphrase) {
    $this->uplinks[$apiKey] = Uplink::create()->requestAccessWithPassphrase($satelliteAddress, $apiKey, $passphrase);
    return $this->uplinks[$apiKey];
  }

  /**
   * @param string $satelliteAddress
   * @param string $apiKey
   * @param string $passphrase
   *
   * @return string
   */
  public function checkUplink(string $satelliteAddress, string $apiKey, string $passphrase) {
    try {
      $access = $this->initializeUplink($satelliteAddress, $apiKey, $passphrase);
      $result = $access->serialize();
    }catch(UplinkException $exc) {
      $result = $exc->getMessage();
    }
    return $result;
  }

  /**
   * Method description.
   */
  public function upload(FileInterface $file) {
    $uuid = Uuid::uuid4();
    $project = $this->access['key']->openProject();
    $project->ensureBucket('raw');
    $upload = $project->uploadObject('raw', $uuid);
    $upload->writeFromResource(fopen("",""));
    $upload->setCustomMetadata([
      StorjFile::METADATA_ATTRIBUTE_NAME => $file->label(),
      StorjFile::METADATA_ATTRIBUTE_MIME => $file->getMimeType(),
      StorjFile::METADATA_ATTRIBUTE_SIZE => $file->getSize(),
      'title' => $file->label(),
      'alt_text' => $file->label(),
      'caption' => $this->t('Caption'),
      'credit' => $this->t('Credit'),
      'width' => $this->t('Width'),
      'height' => $this->t('Height'),
    ]);
    $upload->commit();
  }

  public function download() {
    $project = $this->access['key']->openProject();
    $project->ensureBucket('raw');
    $objectInfo = $project->statObject('raw', $filename);

    try {
      $download = $project->downloadObject('raw', $filename);
      $type = $download->info()->getCustomMetadata()['Content-type'];
    } catch(ObjectNotFound $exc) {

    }
  }

  public function delete() {
    $project = $this->access['key']->openProject();
    $project->ensureBucket('raw');
    $project->deleteObject('raw', $filename);
  }

  public function share() {
    $permissions = new Permission();
    $sharePrefix = new SharePrefix('raw');
    $permissions->allowList(FALSE);
    $this->access['key']->share($permissions, $sharePrefix);

  }

}
