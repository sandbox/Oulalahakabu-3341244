<?php

namespace Drupal\storj\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Storj settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'storj_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['storj.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['satelliteAddress'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Satellite Address'),
      '#default_value' => $this->config('storj.settings')->get('satelliteAddress'),
      '#required' => TRUE,
    ];
    $form['apiKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->config('storj.settings')->get('apiKey'),
      '#required' => TRUE,
    ];
    $form['passphrase'] = [
      '#type' => 'textarea',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->config('storj.settings')->get('passphrase'),
      '#required' => TRUE,
    ];
    $form['check_settings'] = [
      '#type' => 'button',
      '#value' => $this->t('Check settings'),
      '#submit' => ['::checkCallback'],
      '#ajax' => [
        'callback' => '::checkHandler',
        'wrapper' => 'check-storj-wrapper'
      ],
      '#prefix' => '<div id="check-storj-wrapper">',
      '#suffix' => '</div>',
    ];
    $form['bucketName'] = [
      '#type' => 'textfield',
      '#title' => $this->t('bucketName'),
      '#default_value' => $this->config('storj.settings')->get('bucketName'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  public function checkCallback(array &$form, FormStateInterface $form_state) {
  }

  public function checkHandler(array &$form, FormStateInterface $form_state) {
    $check = \Drupal::service('storj.uplink')->checkUplink(
      $form_state->getValue('satelliteAddress'),
      $form_state->getValue('apiKey'),
      $form_state->getValue('passphrase')
    );
    return [
      '#prefix' => '<pre>',
      '#suffix' => '</pre>',
      '#markup' => $check
    ];
  }


  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('example') != 'example') {
      $form_state->setErrorByName('example', $this->t('The value is not correct.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('storj.settings')
      ->set('satelliteAddress', $form_state->getValue('satelliteAddress'))
      ->set('apiKey', $form_state->getValue('apiKey'))
      ->set('passphrase', $form_state->getValue('passphrase'))
      ->set('bucketName', $form_state->getValue('bucketName'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
